# Safety Instructions
***

### 1. Power Supply Cautions
***
* Always make sure that the output voltage of the power supply matches the input voltage of the board.
* While turning on the power supply make sure every circuit is connected properly.
* Do not connect power supply without matching the power rating.
* Never connect a higher output(12V/3Amp) to a lower (5V/2Amp) input .
* Do not try to force the connector into the power socket ,it may damage the connector.

### 2. Device Handling Cautions
***
* Treat every device like it is energized, even if it does not look like it is plugged in or operational.
* While working keep the board on a flat stable surface (wooden table) .
* Unplug the device before performing any operation on them.
* When handling electrical equipment, make sure your hands are dry.
* Keep all electrical circuit contact points enclosed.
* If the board becomes too hot try to cool it with a external usb fan.
* Don’t handle the board when its powered ON.
* Never touch electrical equipment when any part of your body is wet, (that includes fair amounts of perspiration).
* Do not touch any sort of metal to the development board.

### 3. Using GPIO
***
* Always connect the LED (or sensors) using appropriate resistors.
* To Use 5V peripherals with 3.3V we require a logic level converter.
* Never connect anything greater than 5v to a 3.3v pin.
* Do not connect a motor directly , use a transistor to drive it.

### 4. Using UART
***
* Connect Rx pin of device1 to Tx pin of device2 ,similarly Tx pin of device1 to Rx pin of device2.
* If the device1 works on 5v and device2 works at 3.3v, then use the level shifting mechanism(voltage divider).
* UART is used to communicate with board through USB to TTL connection without protection circuit.
* Whereas in _Sensor interfacing_, using UART might require a protection circuit.

### 4. Using SPI
***
* In development boards SPI is in **Push-Pull Mode**, without any protection circuit.
* On Spi interface, if you are using more than one slaves, it is possible that the device2 can "hear" and "respond" to the master's communication with device1, which is a disturbance.
* To overcome this problem , we use a protection circuit with *pull-up resistors* on each of the *Slave Select line(CS)*.
* 1kOhm~10kOhm resistors can be used.
* Generally, 4.7kOhm resistor is used.

### 5. Using I2C
***
* While using I2c interfaces with sensors SDA and SCL lines must be protected.
* Protection of these lines is done by using *pull-up registers* on both lines.
* If you use the in-built pull-up registers in the board, you wont need an external circuit.
* If you are using bread-board to connect your sensor, use the pull-up resistor.
* Generally, 2.2kohm~4Kohm resistors are used.


