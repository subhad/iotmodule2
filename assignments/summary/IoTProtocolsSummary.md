# IIoT Protocols 
***

## 4-20 mA Current Loop
***
* The 4-20 mA current loop has been the standard for signal transmission and electronic control in control systems since the 1950's.
* In Industrial Field, the 4-20 mA Current Loop is well-established, because it is less expensive and does not degrade over long distances, so that **the current signal remains constant through all components in the loop**.
* The accuracy of the signal is not affected by a voltage drop in the interconnecting wiring in 4-20 Loop.
	![Components of 4-20 Current Loop](https://www.predig.com/sites/default/files/images/Indicator/back_to_basics/4-20mA_Current_Loops/4-20mA_current_loop_components.jpg)

### Pros and Cons of 4-20 mA Current Loop
***
#### Pros:
* The 4-20 mA current loop is the dominant standard in many industries.
* It is the simplest option to connect and configure.
* It uses less wiring and connections than other signals, greatly reducing initial setup costs.
* Better for traveling long distances, as current does not degrade over long connections like voltage.
* It is less sensitive to background electrical noise.
* Since 4 mA is equal to 0% output, it is incredibly simple to detect a fault in the system.

#### Cons:
* Current loops can only transmit one particular process signal.
* Multiple loops must be created in situations where there are numerous process variables that require transmission.
* Running so much wire could lead to problems with ground loops if independent loops are not properly isolated.
* These isolation requirements become exponentially more complicated as the number of loops increases.

## Modbus Communication Protocol
***
* Modbus is a communication protocol for transmitting information between electronic devices over serial lines or via the Ethernet and is commonly used in process and factory automation. 
* Modbus serial protocol is a master/slave protocol. _i.e. one master that controls the Modbus data transactions with multiple slaves that respond to the master’s requests to read from or write data to the slaves._
* In a standard Modbus serial network, there is one master and as many as 247 slaves, each with a unique slave address from 1 to 247.
* Communication between a master and a slave occurs in a frame that indicates a function code, which identifies the required action to perform.
* Then the slave responds, based on the function code received.

	![Modbus Protocol](https://realpars.com/wp-content/uploads/2018/12/How-does-Modbus-Communication-Protocol-Work-Between-Devices-1.png)
* Modbus protocol can be used over 2 interfaces
  1. **RS485** - called as Modbus RTU
  2. **Ethernet** - called as Modbus TCP/IP
### RS485
* RS485 is a **serial transmission standard**.
* Several RS485 devices can be put on the same bus.
* RS485 is not directly compatible. _i.e. Correct type of interface must be used or the signals won't go through; mainly done using USB._
	![Modbus TCP/IP and RTU](https://realpars.com/wp-content/uploads/2018/12/Modbus-is-the-Oldest-and-the-Most-Popular-Automation-Protocol.jpg)

## OPCUA
***
* OPC-UA stands for **Open Platform Communications United Architecture**.
* OPC UA is a data exchange standard for industrial communication (machine-to-machine or PC-to-machine communication).
	![OPC-UA Information](/assignments/summary/images/OPC-UAInformation.png)
* OPC_UA is no other than a secure Client-Server Architecture.
	![OPCUA Client-Server Communication](https://www.opc-router.de/wp-content/uploads/2019/08/OPC-UA-security_600x400px_en.png)

## Cloud Protocols
***

### MQTT (Message Queuing Telemetry Transport)
***
* It is a simple messaging protocol, designed for constrained devices with low bandwidth.
* It is lightweight, publish-subscribe network protocol that transports messages between devices.
* Communication between several devices can be established simultaneously.
	![MQTT Connection](https://www.researchgate.net/profile/Ravi_Kodali/publication/316448543/figure/fig5/AS:668593570201611@1536416537854/Establishing-maintaining-and-terminating-MQTT-connection.ppm)
* **MQTT Client**, as a **publisher**, sends a message to the **MQTT broker**, whose work is to distribute the message accordingly to all other *MQTT clients subscribed to the topic*, on which publisher publishes the message.
* **Topics** are a way to register interest for incoming messages or to specify, where to publish the message.
* It is represented by strings, separated by forward slash.
* Each slash indicates a topic level.
	![Example of an application of MQTT Protocol](https://www.opc-router.de/wp-content/uploads/2020/01/MQTT_Schema_EN.jpg)

### HTTP (Hyper Text Transport Protocol)
***
* It is the request-response protocol in Client-Server Architecture.
	![Client-Server Architecture](http://innovationm.co/wp-content/uploads/2016/10/HTTP-Protocol-624x248.png)
* **HTTP Request :** The Request has 3 parts
  1. Request line
  2. HTTP header
  3. Message body
	![HTTP Request](https://mdn.mozillademos.org/files/13687/HTTP_Request.png)
* Some Methods are
  * **GET :** Retrieve the resource from the server (e.g. when visiting a page);
  * **POST :** Create a resource on the server (e.g. when submitting a form);
  * **PUT/PATCH :** Update the resource on the server (used by APIs);
  * **DELETE :** Delete the resource from the server (used by APIs).
* **HTTP Response :** The Response has 3 parts
  1. Request line
  2. HTTP header
  3. Message body
	![HTTP Response](https://mdn.mozillademos.org/files/13691/HTTP_Response.png)
  
	![HTTP Status Codes](/assignments/summary/images/httpStatusCodes.jpg)
