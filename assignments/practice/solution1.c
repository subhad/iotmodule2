/** @file problem1.c
 *  @brief Sending Heartbeat message to MQTT Broker.
 *
 *  Heartbeat messages are those messages that are sent to the cloud to say that the device is alive.
 *  This code fetches heartbeat message in JSON format.
 *  It sends the message to AWS IoT Core by MQTT protocol.
 *
 *  @author Subhadananda Dash
 *  
 */


/* --- Standard Includes --- */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* --- Project Includes --- */
/* --- Includes to get device data for the JSON Message --- */
#include <time.h> //include to get UNIX timestamp

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <unistd.h>
#include <arpa/inet.h>

/* --- Include for Shunya Interfaces APIs --- */
#include <shunyaInterfaces.h>
/* --- Include to connect to MQTT Broker to publish Heartbeat Data --- */
#include "MQTTClient.h"


/** 
 *  @brief
 *  
 *  This function gets the IP address of the UNIX system
 *
 *  @return ip Address in String format
 */

char *getIpAddress(void)
{
    int n;
    struct ifreq ifr;
    char array[] = "eth0";

    n = socket(AF_INET, SOCK_DGRAM, 0);
    //Type of address to retrieve - IPv4 IP address
    ifr.ifr_addr.sa_family = AF_INET;
    //Copy the interface name in the ifreq structure
    strncpy(ifr.ifr_name , array , IFNAMSIZ - 1);
    ioctl(n, SIOCGIFADDR, &ifr);
    close(n);
    //return result
    return(inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr )->sin_addr));
	
}

/** 
 *  @brief
 *  
 *  This function gets the device id from the file /etc/shunya/deviceid of the UNIX system
 *
 *  @return device id as a character array
 */

char *getDeviceId(void)
{
    FILE *fp;
    char dev_id[255];
    fp = fopen("/etc/shunya/deviceid", "r");
    while(fscanf(fp, "%s", dev_id)!=EOF)
    {
	return dev_id;
    }
}

/** 
 *  @brief
 *  
 *  This function gets the current timestamp of the UNIX system
 *
 *  @return UNIX timestamp
 */

long getTimeStamp(void)
{
	time_t seconds;
	seconds = time(NULL); 
	return(seconds);
}

/** 
 *  @brief
 *  
 *  This function is the configuration of connections with AWS IOT core
 *  User gives the all the data
 *
 */

void AWS_config(void){
	"user":{
	"endpoint": " ",
	"port":8883,
	"certificate dir":"/home/shunya/.cert/aws",
	"root certificate":" ",
	"client certificate":" ",
	"private key":" ",
	"client ID":" "
	}
}


/** 
 *  @brief
 *  
 *  This function publishes the heartbeat message in the MQTT Broker.
 *  First create a heartbeat payload, then publish it using Shunya API.
 *
 */

void publish(void)
{
	/* Get the data */
	char *dev_id = getDeviceId();
	char *ipadd = getIpAddress();
	long stp = getTimeStamp();

	/* Code to publish data to MQTT Broker in AWS */
	MQTTClient_message heartbeat = MQTTClient_message_initializer; // initializes message to send on broker
	awsObj user = newAws("user"); // new instance for awsObj
	awsConnectMqtt(&user); // connects to MQTT broker of AWS IOT core

	/* create a payload of heartbeat message */
	heartbeat.payload("device":{
		 "deviceId": dev_id,
		 "timestamp": stp,
		 "eventType": "heartbeat",
		 "ipAddress": ipadd
	}.getBytes());

	/* Publish to topic */
	awsPublishMqtt(&user, "device/heartbeat", "%s" ,heartbeat);
	awsDisconnectMqtt(&user); // release connection
}

/** 
 *  @brief Calls all the function to publish the required heartbeat data in MQTT Broker
 *  
 *  First it initialises the Shunya Interfaces library, then calls the functions serially as shown in the main function.
 *
 *  @return it returns 0, if the program successfully works.
 */

int main (void)
{
	/*Initialisation of library*/
	shunyaInterfacesSetup();
	AWS_config();  //configure the MQTT Broker
	publish();  // publish data in MQTT Broker
	return 0;
}





