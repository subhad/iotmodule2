/** @file problem2.c
 *  @brief 
 *
 *  Code for reading data from the RISHI Meter 3430 using Shunya Interfaces modbus API's.
 *  Current sensing, voltage sensing and power sensing units of the meter will give us data for required parameters.
 *  Fetching data for the required parameters from these units using an IoT Gateway and MODBUS (RS485) interface.
 *
 *  @author Subhadananda Dash
 *  
 */


/* --- Standard Includes --- */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* --- Include for Shunya Interfaces APIs --- */
#include <shunyaInterfaces.h>


int main(void)
{
	/* Initialisation of library */
	initlib();

	/* modbus RTU configuration for RS485 */

	"modbus-rtu": {
	    "type": "rtu",
	    "device": "/dev/ttyAMA0",
	    "baudrate": 9600,
	},


	modbusObj x = newModbus("modbus-rtu"); // create new instance of MODBUS
	modbusRtuConnect(&x); // connect with modbus

	float ac = modbusRtuRead(&x, 30007); 			// reading AC-Current
	float voltage = modbusRtuRead(&x, 30001);    		// reading voltage
	float power = modbusRtuRead(&x, 30013);           	// reading power
	float active_energy = modbusRtuRead(&x, 30147);   	// reading active_energy
	float reactive_energy = modbusRtuRead(&x, 30151);	// reading reactive energy
	float apparent_energy = modbusRtuRead(&x, 30081);	// reading apparent energy
	float THD = modbusRtuRead(&x, 30219);			// reading the THD of voltage
	
	/* Disconnect to the Serial device*/
	modbusRtuDisconnect(&x);

	return 0;
}


