# Problem 2
### Purpose : Write code for reading data from the RISHI Meter 3430 using Shunya Interfaces modbus API's

## Data that needs to be read from the device
1. AC current 
1. Voltage 
1. Power
1. Active energy (kWhr), 
1. Reactive energy (kVArhr) 
1. Apparent energy (kVAhr) 
1. THD of voltage

* Current sensing, voltage sensing and power sensing units of the meter will give us data for required parameters. 
* Fetch data for the required parameters from these units using an IoT Gateway and the following interface.
    * Interface -
        * **MODBUS (RS485)**
        * Addresses to read data from: 

        | Parameter | Modbus address |
        | ------ | ------ |
        | AC current | 30007 |
        | Voltage | 30001 | 
        | Power | 30013 | 
        | Active energy | 30147 | 
        | Reactive energy | 30151 | 
        | Apparent energy | 30081 | 
        | THD of Voltage | 30219 | 


### Description
- For Meter connections using RS485 assume that the meter will be connected to uart device node `/dev/ttyAMA0` 


### Acceptance Criteria 
This is the criteria to accept the assignment.
- The code must be divided into functions 
- **Must** have documentation for using the program.
- Code **Must** follow the Coding Standards.
- Code must be in C.
- Code must be compiled in the Shunya OS docker container.

#### Note: Documentation of the whole program is a must criteria for the completion of the Assignment.
